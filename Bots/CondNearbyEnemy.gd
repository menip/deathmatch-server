extends "res://addons/godot-behavior-tree-plugin/condition.gd"


# Leaf Node
func tick(tick):
	if tick.actor.visible_enemies.size() == 0:
		return OK 
	return FAILED