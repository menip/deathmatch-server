extends "res://addons/godot-behavior-tree-plugin/action.gd"

# Leaf Node
func tick(tick):
	# TODO: Actaully get nearest player
	var target_pos = tick.actor.last_known_enemy_loc.values()[0]
	tick.actor.bot_velocity = (target_pos - tick.actor.position).normalized()
	
	if (target_pos - tick.actor.position).length() < 200:
		tick.actor.last_known_enemy_loc.erase(tick.actor.last_known_enemy_loc.keys()[0])
	
	tick.actor.get_node("Status").text = "Seeking"
