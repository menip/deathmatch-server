extends "res://addons/godot-behavior-tree-plugin/action.gd"

# Leaf Node
func tick(tick):
	# TODO: Actaully get nearest player
	var nearest_player = tick.actor.visible_enemies.back() 
	tick.actor.bot_velocity = (nearest_player.position - tick.actor.position).normalized()
	tick.actor.get_node("Status").text = "Approaching"
	
	return FAILED # Continue on to Fire