extends "res://addons/godot-behavior-tree-plugin/action.gd"


func tick(tick):
	# TODO: Actually look at who is nearest enemy
	var nearest_enemy = tick.actor.visible_enemies.back() # Grab last for now
	tick.actor.bot_velocity = -1 * (nearest_enemy.position - tick.actor.position).normalized()
	tick.actor.get_node("Status").text = "Running away"
	return FAILED # So can look at new pickups next tick