extends "res://addons/godot-behavior-tree-plugin/condition.gd"


# Leaf Node
func tick(tick):
	if tick.actor.last_known_enemy_loc.size() == 0:
		return OK # Nobody to look for, just exit
	return FAILED # Go seek out player
