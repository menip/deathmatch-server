extends "res://addons/godot-behavior-tree-plugin/action.gd"


# Leaf Node
func tick(tick):
	var target_pickup = null
	
	if tick.actor.pickups_health.size() > 0:
		# TODO: get closest
		target_pickup = tick.actor.pickups_health.back()
	elif tick.actor.pickups_shield.size() > 0:
		# TODO: get closest
		target_pickup = tick.actor.pickups_shield.back()
	
	if target_pickup != null:
		tick.actor.bot_velocity = (target_pickup.position - tick.actor.position).normalized()
		tick.actor.get_node("Status").text = "Getting health"
		return ERR_BUSY # Exit this tick
	else:
		# If no known pickups, just continue to next one, hopefully we see one soon
		return FAILED