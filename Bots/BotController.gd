extends Node2D

var bot:KinematicBody2D = null
var bot_velocity:Vector2

var pickups_health:Array
var pickups_shield:Array
var pickups_ammo:Array

var visible_enemies:Array
var last_known_enemy_loc:Dictionary

var is_firing:bool = false
var bot_target:Vector2

var idx = -1


func bot_init():
	bot.connect("died", self, "_on_bot_die")
	
	bot_velocity = Vector2()
	
	pickups_health = []
	pickups_shield = []
	pickups_ammo = []
	
	visible_enemies = []
	last_known_enemy_loc = {}
	
	set_process(true)


func _process(delta):
	bot_velocity = Vector2()
	is_firing = false
	position = bot.position
	$BehaviorTree.tick(self, $BehaviorBlackboard)
	
	bot.velocity = bot_velocity * bot.SPEED_MOVE
	bot.rotation = bot_velocity.angle() + PI/2
	
	# Have gun point at target
	if is_firing and bot_target != null:
		bot.rotation = (bot_target - bot.gun.global_position).normalized().angle() + PI/2
	
	bot.rpc("update_vals", bot.position, bot.velocity, bot.rotation)
	
	if is_firing and bot.gun.get_node("FireDelay").is_stopped():
		bot.gun.fire()


func _on_Vision_body_entered(body):
	if body != bot:
		visible_enemies.append(body)
		
		if last_known_enemy_loc.has(body.name):
			last_known_enemy_loc.erase(body.name)


func _on_Vision_body_exited(body):
	if body != bot:
		visible_enemies.erase(body)
		
		last_known_enemy_loc[body.name] = body.position


func _on_Vision_area_entered(area):
	if area is Pickup:
		match area.crate_id:
			0: pickups_ammo.append(area)
			1: pickups_health.append(area)
			2: pickups_shield.append(area)


func _on_Vision_area_exited(area):
	if area is Pickup:
		match area.crate_id:
			0: pickups_ammo.erase(area)
			1: pickups_health.erase(area)
			2: pickups_shield.erase(area)

func _on_bot_die():
	bot = null
	position = Vector2(-1000, -1000) # Move off of game world
	set_process(false)