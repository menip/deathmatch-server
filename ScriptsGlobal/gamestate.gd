extends Node

# Default game port
const DEFAULT_PORT = 55555

# Max number of players
const MAX_PEERS = 12

# Names for remote players in id:name format
var players = {}

# Signals to let lobby GUI know what's going on
signal game_ended()
signal game_error(what)


func _ready():
	print("gamestate ready")
	#randomize() # Uncomment when deploying
	
	var host = NetworkedMultiplayerENet.new()
	host.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(host)
	
	globals.setup_refs()
	
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")


# Callback from SceneTree
func _player_connected(id):
	globals.rpc_id(id, "set_data", globals.data) # Send to player
	
	# Add everyone to new player:
	rset_id(id, "players", players)


# Callback from SceneTree
func _player_disconnected(id):
	unregister_player(id)
	
	if globals.node_Players.has_node("player_" + String(id)):
		globals.node_world.remove_player(id)


# Lobby management functions
func register_player(id, new_player_name):
	# Clean string just a tad, limit at 10 chars
	new_player_name = new_player_name.strip_edges()
	new_player_name = new_player_name.substr(0,10)
	
	# Add him to our list
	if not players.has(id):
		players[id] = {"name":new_player_name, "total_kills":0, "kill_streak":0}
	else:
		players[id]["name"] = new_player_name
	
	rset("players", players) # Sync everyone to new players list
	# NOTE: this means new player's register gets called twice, but fine as same info sent both times


puppetsync func unregister_player(id):
	players.erase(id)


remote func spawn_player(player_name, gun_id, shield_id, ability_id):
	var caller_id = get_tree().get_rpc_sender_id()
	
	if not globals.node_Players.has_node(String(caller_id)):
		register_player(caller_id, player_name)
		# TODO: Could check that spawn request is valid
		var spawn_pos = Vector2(randf() * globals.BOUNDS.size.x, randf() * globals.BOUNDS.size.y)
		globals.node_world.spawn_player(spawn_pos, caller_id, gun_id, shield_id, ability_id)


func spawn_bot(bot_id, bot_name, gun_id, shield_id, ability_id):
		register_player(bot_id, bot_name)
		var spawn_pos = Vector2(randf() * globals.BOUNDS.size.x, randf() * globals.BOUNDS.size.y)
		return globals.node_world.spawn_player(spawn_pos, bot_id, gun_id, shield_id, ability_id, true)

remote func ready_to_start():
	var caller_id = get_tree().get_rpc_sender_id()
	
	# Add everyone to new player. NOTE: Similar code runs when player joins play field:
	for p_id in players:
		rpc_id(caller_id, "register_player", p_id, players[p_id]) # Send player to new dude
	
	for bullet in globals.node_bullet_container.get_children():
		globals.node_world.rpc_id(caller_id, "spawn_bullet", bullet.position, bullet.rotation, bullet.id_origin, bullet.name, bullet.ammo_id)
	
	for player in globals.node_Players.get_children():
		# Yea yea, it's ugly
		var is_bot = false
		if player.name.begins_with("bot"):
			is_bot = true
		
		globals.node_world.rpc_id(caller_id, "spawn_player", player.position, player.id, 
					player.get_node("GunAnchor/Gun").gun_id, player.get_node("Shield").shield_id, player.ability_id, is_bot)
		player.rpc_id(caller_id, "health_change", player.health)
		player.get_node("Shield").rpc_id(caller_id, "shield_change", player.get_node("Shield").health)
	
	for pickup in globals.node_world.get_node("Pickups").get_children():
		globals.node_world.rpc_id(caller_id, "spawn_crate", pickup.position, pickup.crate_id, pickup.name, pickup.is_hidden)
	
	rpc_id(caller_id, "post_start_game")


# Returns list of player names
func get_player_list():
	var names = []
	for id in players:
		names.append(players[id]["name"])
	
	return names


func add_kill(id_killer, id_killed):
	players[id_killer]["total_kills"] += 1
	players[id_killer]["kill_streak"] += 1
	
	players[id_killed]["kill_streak"] = 0
	
	rpc("add_kill", id_killer, id_killed)


remote func player_exit_game():
	var caller_id = get_tree().get_rpc_sender_id()
	
	unregister_player(caller_id)
	
	if globals.node_Players.has_node("player_" + String(caller_id)):
		globals.node_world.remove_player(caller_id)


# So clients can know latency
remote func ping():
	var caller_id = get_tree().get_rpc_sender_id()
	rpc_id(caller_id, "pong")


remote func combatant_count():
	var caller_id = get_tree().get_rpc_sender_id()
	
	var counts = globals.node_world.combatant_counts()
	rset("player_count", counts["players"])
	rset("bot_count", counts["bots"])
	

