extends Node

class_name Gaussian


func _ready():
	_test()

static func gaussian(x: float, amplitude: float, std:float) -> float:
	var variance = 2 * std * std
	
	return amplitude * exp(-(x*x)/variance)


static func gaussian_2d(point:Vector2, shift:Vector2 = Vector2(0,0), amplitude:float = 1, std:Vector2 = Vector2(1,1)) -> float:
	var variance = Vector2(2 * pow(std.x, 2), 2 * pow(std.y, 2))
	var exponent = pow((point.x - shift.x), 2) / variance.x + pow((point.y - shift.y), 2) / variance.y
	
	return amplitude * exp(-exponent)


# Samples gauss function in specific range, returns sum of samples.\
# Samples from center of tile
static func expected_value_2d(init:Vector2, final:Vector2, tile_size:float = 0.4,
		shift:Vector2 = Vector2(0,0), amplitude:float = 1, std:Vector2 = Vector2(1,1)) -> float:
	
	var expected_value = 0
	for x in range(init.x, final.x, tile_size):
		for y in range(init.y, final.y, tile_size):
			expected_value += gaussian_2d(Vector2(x,y), shift, amplitude, std)
	
	return expected_value


static func _test():
	randomize()
	var map_size = 1000
	var tile_size = 128
	var desired_spawn_count = 5
	var shift = Vector2(map_size/2, map_size/2)
	var std = Vector2(map_size * .35, map_size * .35)
	
	var expected_value = expected_value_2d(Vector2(), Vector2(map_size, map_size), tile_size, Vector2(map_size/2, map_size/2), .7, Vector2(map_size * .35, map_size * .35))
	print("\nExpected: ", expected_value)
	
	var actuals = []
	var n = 1000
	var i = 0
	while i < n:
		var spawned_val = 0
		for y in range(0, map_size, tile_size):
			var row = []
			for x in range(0, map_size, tile_size):
				var point = Vector2(x + tile_size/2, y + tile_size/2)
				#var gauss_val = gaussian_2d(point, shift, .7, std)											# Vanila
				#var gauss_val = gaussian_2d(point, shift, .7/expected_value, std) 							# With ev factored in
				var gauss_val = gaussian_2d(point, shift, (.7/expected_value)*desired_spawn_count, std) 	# With desired count also factored in
				
				if randf() < gauss_val:
					spawned_val += 1
				
				row.append(gauss_val)
			#print(row)
		actuals.append(spawned_val)
		i += 1
	
	var mean = 0.0
	for val in actuals:
		mean += val
	mean = mean / actuals.size()
	
	if n <= 10:
		print("Actuals: ", actuals)
	print("Iterations: ", n, " Mean: ", mean, "\n")