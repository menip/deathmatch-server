extends Node

var node_world = null
var node_Players = null
var node_bullet_container = null

const BOUNDS = Rect2(0, 0, 2000, 2000)

onready var data:Dictionary = load_data("res://Assets/Database_Items.json")


func setup_refs():
	node_world = get_tree().get_root().get_node("World")
	node_Players = node_world.get_node("Players")
	node_bullet_container = node_world.get_node("Bullets")


func load_data(path):
	var file = File.new()
	
	if path == null:
		return {}
	if !file.file_exists(path):
		return {}
	
	file.open(path, File.READ)
	var data = {}
	data = parse_json(file.get_as_text())
	file.close()
	return data

