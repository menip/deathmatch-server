extends Area2D
class_name Shield

export(String) var description = ""

export(float) var MAX_HEALTH = 100.0
export(float) var RECHARGE_RATE = 1.0
var health = 100.0


func _ready():
	health = MAX_HEALTH
	rpc("shield_change", health)


func _process(delta):
	# Only regen shield if it's up and below max.
	if not health <= 0 and health < MAX_HEALTH:
		health += RECHARGE_RATE * delta
		
		if health > MAX_HEALTH:
			health = MAX_HEALTH
		
		rpc("shield_change", health)


func refresh():
	health = MAX_HEALTH
	rpc("shield_change", health)
	update_aliveness()


func hit(damage_ammount):
	health -= damage_ammount
	
	if health < 0:
		health = 0
	
	update_aliveness()
	rpc("shield_change", health)


func shield_pickup(shield_ammount):
	if health <= 0:
		health = shield_ammount
	else: 
		health += shield_ammount
	
	if health > MAX_HEALTH:
		health = MAX_HEALTH
	
	update_aliveness()
	rpc("shield_change", health)


func update_aliveness():
	$Sprite.modulate = Color.white.linear_interpolate(Color.blue, 1 - health/MAX_HEALTH) 

	if health <= 0:
		hide()
		$CollisionShape2D.set_deferred("disabled", true)
	else:
		show()
		$CollisionShape2D.set_deferred("disabled", false)