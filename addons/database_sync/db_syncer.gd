tool
extends EditorPlugin

# Plugin tests that vals like bullet count, health, etc are synced between scenes and json


onready var data:Dictionary = load_data("res://Assets/Database_Items.json")


# Initialize plugin here
func _enter_tree():
	add_tool_menu_item("Test synced", self, "test_synced")
	add_tool_menu_item("Sync to file", self, "sync_to_file")
	add_tool_menu_item("Sync from file", self, "sync_from_file")
	
	var play_button = get_editor_interface().get_base_control().get_child(1).get_child(0).get_child(4).get_child(0)
	play_button.connect("pressed", self, "on_play_pressed")
	
	var stop_button = get_editor_interface().get_base_control().get_child(1).get_child(0).get_child(4).get_child(2)
	stop_button.connect("pressed", self, "on_stop_pressed")


# Clean up plugin here
func _exit_tree():
	remove_tool_menu_item("Test synced")
	remove_tool_menu_item("Sync to file")
	remove_tool_menu_item("Sync from file")
	
	# Do I need to remove ref stored by play/stop_button?



func on_play_pressed():
	if test_synced(null) == false:
		get_editor_interface().get_base_control().modulate = Color.lightpink
		assert(false)

func on_stop_pressed():
	get_editor_interface().get_base_control().modulate = Color.white


func test_synced(some_arg):
	data = load_data("res://Assets/Database_Items.json")
	
	var synced = true
	
	# Test weapons
	for weapon in data["weapons"]:
		var d_weapon = data["weapons"][weapon]
		var instance = (load(d_weapon["scene"])).instance()
		
		if instance.MAX_BULLETS != d_weapon["bullets"]:
			synced = false
			print(instance.filename, " bullets not synced")
		if instance.get_node("FireDelay").wait_time != d_weapon["fire_rate"]:
			synced = false
			print(instance.filename, " fire rate not synced")
		if instance.description != d_weapon["description"]:
			synced=false
			print(instance.filename, " description not synced")
		instance.queue_free()
	
	
	# Test shield
	for shield in data["shields"]:
		var d_shield = data["shields"][shield]
		var instance = (load(d_shield["scene"])).instance()
		
		if instance.MAX_HEALTH != d_shield["health"]:
			synced = false
			print(instance.filename, " health not synced")
		if instance.RECHARGE_RATE != d_shield["regen_rate"]:
			synced = false
			print(instance.filename, " regen rate not synced")
		if instance.description != d_shield["description"]:
			synced=false
			print(instance.filename, " description not synced")
		instance.queue_free()
	
	# Test ammo. Note not all are projectiles
	for ammo in data["ammo"]:
		var d_ammo = data["ammo"][ammo]
		if d_ammo.has("bullet_speed"):
			var instance = (load(d_ammo["scene"])).instance()
			if instance.BULLET_SPEED != d_ammo["bullet_speed"]:
				synced = false
				print(instance.filename, " speed not synced")
			instance.queue_free()
		# Test Laser Beam size synced
		if ammo == "3":
			var instance = (load(d_ammo["scene"])).instance()
			if instance.INIT_LASER_SIZE != d_ammo["laser_size"]:
				synced = false
				print(instance.filename, " laser size not synced")
			instance.queue_free()
	
	# Test play area sync
	if globals.BOUNDS.size.x != data["play_size"]["x"] or globals.BOUNDS.size.y != data["play_size"]["y"]:
		synced = false
		print("Bounds not synced")
	
	# Test player synced
	var instance = (load("res://Player/Player.tscn")).instance()
	if instance.MAX_HEALTH != data["player"]["health"]:
		synced = false
		print("Player health not synced")
	if instance.SPEED_MOVE != data["player"]["speed_move"]:
		synced = false
		print("Player speed not synced ")
	if instance.TELLEPORT_RANGE != data["abilities"]["1"]["range"]:
		synced = false
		print("Player telleport range not synced")
	instance.queue_free()
	
	print("Synced: ", synced)
	return synced


func sync_to_file(some_arg):
	data = load_data("res://Assets/Database_Items.json")
	
	# Sync shield health and regen
	for weapon in data["weapons"]:
		var d_weapon = data["weapons"][weapon]
		var instance = (load(d_weapon["scene"])).instance()
		
		if instance.MAX_BULLETS != d_weapon["bullets"]:
			d_weapon["bullets"] = instance.MAX_BULLETS
			print(instance.filename, " bullets synced")
		if instance.get_node("FireDelay").wait_time != d_weapon["fire_rate"]:
			d_weapon["fire_rate"] = instance.get_node("FireDelay").wait_time
			print(instance.filename, " fire rate synced")
		if instance.description != d_weapon["description"]:
			d_weapon["description"] = instance.description
			print(instance.filename, " description synced")
		instance.queue_free()
	
	# Sync shield health and regen
	for shield in data["shields"]:
		var d_shield = data["shields"][shield]
		var instance = (load(d_shield["scene"])).instance()
		
		if instance.MAX_HEALTH != d_shield["health"]:
			d_shield["health"] = instance.MAX_HEALTH
			print(instance.filename, " health synced")
		if instance.RECHARGE_RATE != d_shield["regen_rate"]:
			d_shield["regen_rate"] = instance.RECHARGE_RATE
			print(instance.filename, " regen rate synced")
		if instance.description != d_shield["description"]:
			d_shield["description"] = instance.description
			print(instance.filename, " description synced")
		instance.queue_free()
	
	# Sync ammo sync. Note not all are projectiles
	for ammo in data["ammo"]:
		var d_ammo = data["ammo"][ammo]
		if d_ammo.has("bullet_speed"):
			var instance = (load(d_ammo["scene"])).instance()
			if instance.BULLET_SPEED != d_ammo["bullet_speed"]:
				d_ammo["bullet_speed"] = instance.BULLET_SPEED
				print(instance.filename, " speed synced")
			instance.queue_free()
		# Sync Laser Beam size
		if ammo == "3":
			var instance = (load(d_ammo["scene"])).instance()
			if instance.INIT_LASER_SIZE != d_ammo["laser_size"]:
				d_ammo["laser_size"] = instance.laser_size
				print(instance.filename, " laser size synced")
			instance.queue_free()
	
	# Sync play area sync
	if globals.BOUNDS.size.x != data["play_size"]["x"] or globals.BOUNDS.size.y != data["play_size"]["y"]:
		data["play_size"]["x"] = globals.BOUNDS.size.x
		data["play_size"]["y"] = globals.BOUNDS.size.y
		print("Bounds synced")
	
	# Sync player synced
	var instance = (load("res://Player/Player.tscn")).instance()
	if instance.MAX_HEALTH != data["player"]["health"]:
		data["player"]["health"] = instance.MAX_HEALTH
		print("Player health synced")
	if instance.SPEED_MOVE != data["player"]["speed_move"]:
		data["player"]["speed_move"] = instance.SPEED_MOVE
		print("Player speed synced ")
	if instance.TELLEPORT_RANGE != data["abilities"]["1"]["range"]:
		data["abilities"]["1"]["range"] = instance.TELLEPORT_RANGE
		print("Player telleport range synced")
	instance.queue_free()
	
	write_data(to_json(data), "res://Assets/Database_Items.json")
	print("All data synced")


func sync_from_file(some_arg):
	print("Sync from file yourself, I don't wanna be parsing and editing scripts. Here is list of descrepencies: ")
	test_synced(null)


func write_data(data, path):
	var file = File.new()
	
	assert(path != null)
	file.open(path, File.WRITE)
	file.store_string(data)
	file.close()


func load_data(path):
	var file = File.new()
	
	if path == null:
		return {}
	if !file.file_exists(path):
		return {}
	
	file.open(path, File.READ)
	var data = {}
	data = parse_json(file.get_as_text())
	file.close()
	return data

