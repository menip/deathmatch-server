extends KinematicBody2D

const SPEED_MOVE = 200.0
var move_speed

const MAX_HEALTH = 100.0
const TELLEPORT_RANGE = 600

var health = 100.0
var id = -1

signal died
var dead = false

var ability_id = "-1" # Cause in data stored as string :shrug:

var velocity = Vector2()

var gun

func _ready():
	move_speed = SPEED_MOVE
	gun = $GunAnchor/Gun

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move_and_collide(velocity * delta) 
	
	if position.x < 0:
		position.x = 0
	elif position.x > globals.BOUNDS.size.x:
		position.x = globals.BOUNDS.size.x
	if position.y < 0:
		position.y = 0
	elif position.y > globals.BOUNDS.size.y:
		position.y = globals.BOUNDS.size.y


remote func do_ability(telleport_pos = Vector2(0,0)):
	var caller_id = get_tree().get_rpc_sender_id()
	
	if caller_id == id:
		if $AbilityCooldown.is_stopped():
			match ability_id:
				"0":
					# Reset health
					health = MAX_HEALTH
					rpc("health_change", health)
					
					gun.refresh()
					$Shield.refresh()
				"1":
					var telleport_dir = telleport_pos - position
					position += telleport_dir.clamped(TELLEPORT_RANGE)
					rpc("update_vals", Vector2(), Vector2(), rotation)
				"2":
					$InvisDuration.start()
					rpc("toggle_invis", true)
					rpc("update_movespeed", (1 + globals.data["abilities"]["2"]["percent_move_speed_bonus"]/100) * SPEED_MOVE)
					move_speed = 1.5 * SPEED_MOVE
			$AbilityCooldown.start()
			rpc_id(id, "ability_cd_start")


remote func update_vals(move_dir, _rotation):
	var caller_id = get_tree().get_rpc_sender_id()
	
	if caller_id == id:
		velocity = move_dir.normalized() * move_speed
		$GunAnchor.rotation = _rotation
		rpc("update_vals", position, velocity, _rotation)
	

func set_player_name(new_name):
	get_node("you").set_text(new_name)


func set_gun(gun, offset):
	if has_node("GunAnchor/Gun"): # When would this ever be the case?
		gun.queue_free()
	
	gun.set_name("Gun")
	gun.position = offset
	$GunAnchor.add_child(gun)
#	move_child(gun, 0)


func set_shield(shield):
	if has_node("Shield"):
		$Shield.queue_free()
	
	shield.set_name("Shield")
	add_child(shield)
	move_child(shield, 0)


func ammo_pickup(ammount):
	gun.ammo_pickup(ammount)


func shield_pickup(ammount):
	$Shield.shield_pickup(ammount)


func health_pickup(ammount):
	health += ammount
	
	if health > MAX_HEALTH:
		health = MAX_HEALTH
	
	rpc("health_change", health)


func hit(by_who_id, damage_ammount):
	# TODO: Add number health things
	health -= damage_ammount
	rpc("health_change", health)
	if health <= 0 and not dead: # Cases were multiple bullets hit same frame
		dead = true
		rpc("die", by_who_id)


# Why it puppetsync?
puppetsync func die(by_who_id):
	gamestate.add_kill(by_who_id, id)

	gun.on_queue_free()
	emit_signal("died")
	queue_free() # Get rid of player on all peers



func _on_InvisDuration_timeout():
	rpc("toggle_invis", false)
	rpc("update_movespeed", SPEED_MOVE)
	move_speed = SPEED_MOVE
