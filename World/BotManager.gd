extends Node

var BotController = preload("res://Bots/BotController.tscn")

var bot_idx = 0 # Keep track # bots spawned to insure unique node name
var controller_idx = 0

var names = ["Alice", "Bob", "Cole", "Dan", "Even", "Finly", "George", "Heather", "Ian", "Jojo", "Kate", "Lily", "Madie"]

func create_bot():
	var next_controller = null
	
	# Check for existing bot controllers without bot
	for bot_controller in get_children():
		if bot_controller.bot == null:
			next_controller = bot_controller
			break
	
	if next_controller == null:
		next_controller = spawn_controller()
	
	next_controller.bot = spawn_bot(next_controller.idx)
	next_controller.bot_init()


func spawn_controller():
	var controller = BotController.instance()
	controller.idx = controller_idx
	controller_idx += 1
	add_child(controller)
	
	return controller


func spawn_bot(_idx):
	bot_idx += 1
	
	var bot_name = "0xDEADBEEF"
	if names.size() > _idx:
		bot_name = names[_idx]
	
	var gun_id = randi() % 4
	var shield_id = randi() % 2
	return gamestate.spawn_bot(_idx, bot_name, String(gun_id), String(shield_id), "0")

