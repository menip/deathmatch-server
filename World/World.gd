extends Node2D

var bullet_idx = 0 # Hack to have bullets same name across server/client
var crate_idx = 0 # Hack to have crates same name across server/client

# For spawning pickups
var tile_size = 64
var shift = globals.BOUNDS.size / 2
var std = globals.BOUNDS.size * 0.25 # ~60% of pickups should be spawning within 25% of center of map
var max_spawn_chance = .6
var spawned_matrix = []
var crate_counts = [0,0,0]

var target_players = 3

var player_count:int = 0
var bot_count:int = 0

func _ready():
	gamestate.connect("game_error", self, "_on_game_error")
	
	#randomize() # Uncomment when deploying
	
	# Is there better way to do this?
	var i = 0
	while i < int(globals.BOUNDS.size.x/tile_size + 1) * int(globals.BOUNDS.size.y/tile_size + 1):
		spawned_matrix.append(false)
		i += 1
	
	_on_TimerPickupSpawn_timeout()


# Call with where to spawn and id of player spawning
func spawn_player(spawn_pos, spawn_id, gun_id, shield_id, ability_id, is_bot=false):
	var player_scene = load("res://Player/Player.tscn")
	var gun_scene = load(globals.data["weapons"][String(gun_id)]["scene"])
	var shield_scene = load(globals.data["shields"][String(shield_id)]["scene"])
	
	# Spawn this one on all peers, setting network master as appropriate
	var player = player_scene.instance()
	var gun = gun_scene.instance()
	var shield = shield_scene.instance()
	
	var offset = Vector2( 	globals.data["weapons"][String(gun_id)]["offset_x"],
							globals.data["weapons"][String(gun_id)]["offset_y"])
	
	gun.id_player = spawn_id
	player.set_gun(gun, offset)
	player.set_shield(shield)
	player.ability_id = String(ability_id)
	player.get_node("AbilityCooldown").wait_time = globals.data["abilities"][ability_id]["cooldown"]
	
	# Imp: Set unique node name
	if is_bot:
		player.name = "bot_" + str(spawn_id)
		bot_count += 1
	else: 
		player.name = "player_" + str(spawn_id)
		player_count += 1
	
	player.id = spawn_id
	player.position = spawn_pos
	
	player.set_player_name(gamestate.players[spawn_id]["name"])
	
	globals.node_Players.add_child(player)
	
	player.connect("died", self, "on_combatant_died", [player])
	
	rpc("spawn_player", spawn_pos, spawn_id, gun_id, shield_id, ability_id, is_bot)
	
	return player


func spawn_crate(spawn_pos, crate_id, hidden=false):
	var row = (spawn_pos.y - tile_size/2)/tile_size
	var column = (spawn_pos.x - tile_size/2)/tile_size
	var index = row * int(globals.BOUNDS.size.y/tile_size + 1) + column
	
	if not spawned_matrix[index]:
		var crate_scene = load(globals.data["pickups"][String(crate_id)]["scene"])
		var crate = crate_scene.instance()
		
		crate.position = spawn_pos
		crate.name = String(crate_idx)
		if hidden:
			crate.pickup() # Hides
		crate.connect("pickup", self, "_on_pickup")
		globals.node_world.get_node("Pickups").add_child(crate)
		rpc("spawn_crate", spawn_pos, crate_id, crate_idx)
		
		crate_idx += 1
		crate_counts[int(crate_id)] += 1
		spawned_matrix[index] = true


func spawn_bullet(pos, rot, id, ammo_id):
	var ammo_scene = load(globals.data["ammo"][String(ammo_id)]["scene"])
	var ammo = ammo_scene.instance()
	ammo.name = String(bullet_idx)
	ammo.position = pos
	ammo.rotation = rot
	ammo.set_id_origin(id)
	globals.node_bullet_container.add_child(ammo)
	
	rpc("spawn_bullet", pos, rot, id, bullet_idx, ammo_id)
	
	bullet_idx += 1 # Increment so next bullet name unique too


func remove_player(id):
	var player = $Players.get_node("player_" + String(id))
	
	var gun = player.get_node("GunAnchor/Gun")
	if gun.ammo_id == 3 and gun.laser_beam != null:
		gun.on_queue_free()
	
	$Players.remove_child(player)
	rpc("remove_player", id)
	
	player.queue_free()
	player_count -= 1


func _on_game_error(errtxt):
	get_node("error").dialog_text = errtxt
	get_node("error").popup_centered_minsize()


# Spawns more pickups as needed
func _on_TimerPickupSpawn_timeout():
	# TODO: Also spawn bots here, just for now :wink:
	if $Players.get_child_count() < target_players:
		for i in range(target_players - $Players.get_child_count()): # So target of 3 players on field
			$BotControllers.create_bot()
	
	var expected_value = Gaussian.expected_value_2d(Vector2(), globals.BOUNDS.size, tile_size, shift, max_spawn_chance, std)
	
	# Note: Spawns under desired count as some point may of been occupied already. It's fine
	var desired_spawn_count = 2 * $Players.get_child_count() - $Pickups.get_child_count() + 15# Some functions that considers number of players
	var tailored_amplitude = (max_spawn_chance/expected_value) * desired_spawn_count
	
	if desired_spawn_count > 0:
		for x in range(0, globals.BOUNDS.size.x, tile_size):
			for y in range(0, globals.BOUNDS.size.y, tile_size):
				var point = Vector2(x + tile_size/2, y + tile_size/2) # At center of tile
				var gauss_val = Gaussian.gaussian_2d(point, shift, tailored_amplitude, std) 
				
				if randf() < gauss_val:
					var crate_type = 0
					if crate_counts[1] < crate_counts[crate_type]: 
						crate_type = 1
					if crate_counts[2] < crate_counts[crate_type]: 
						crate_type = 2
					
					spawn_crate(point, crate_type)


func _on_pickup(at_position, crate_type):
	var row = (at_position.y - tile_size/2)/tile_size
	var column = (at_position.x - tile_size/2)/tile_size
	var index = row * int(globals.BOUNDS.size.y/tile_size + 1) + column
	
	if spawned_matrix[index]: # May be some bug when two players reach crate similar time...?
		spawned_matrix[index] = false
		crate_counts[int(crate_type)] -= 1

func combatant_counts():
	return {"players": player_count, "bots": bot_count}


func on_combatant_died(who):
	if who.name.begins_with("b"):
		bot_count -= 1
	else:
		player_count -= 1

