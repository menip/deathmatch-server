extends Area2D
class_name Ammo

var ammo_id
var id_origin = -1


func set_id_origin(id):
	id_origin = id


func remove_bullet():
	rpc("remove_bullet")
	
	queue_free()

