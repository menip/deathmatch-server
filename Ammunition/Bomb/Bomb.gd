extends Ammo

var damage = 50


func _ready():
	ammo_id = 2


func _on_Bomb_body_entered(body):
	if body.has_method("hit") and body.id != id_origin:
		body.hit(id_origin, damage)
		remove_bullet()


# Currently don't care who shoot at shields
func _on_Bomb_area_entered(area):
	if area.has_method("hit") and area.get_parent().id != id_origin:
		area.hit(damage)
		remove_bullet()


func _on_BombExpire_timeout():
	remove_bullet()

