extends Ammo

const BULLET_SPEED = 500.0
var direction = Vector2()

var damage = 20


func _ready():
	direction = Vector2(cos(rotation - PI/2), sin(rotation - PI/2))
	
	ammo_id = 0


func _process(delta):
	position += direction * BULLET_SPEED * delta
	# NOTE: Should sync bullet pos?
	
	# Check if out of play field
	if not globals.BOUNDS.has_point(position):
		remove_bullet()


func _on_Rocket_body_entered(body):
	if body.has_method("hit"):
		body.hit(id_origin,damage)
		remove_bullet()


# Currently don't care who shoot at shields
func _on_Rocket_area_entered(area):
	if area.has_method("hit"):
		area.hit(damage)
		remove_bullet()


func _on_FlightDuration_timeout():
	remove_bullet()

