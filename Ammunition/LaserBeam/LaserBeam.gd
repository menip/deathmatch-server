extends Ammo

var direction = Vector2()

var damage_per_tick = 5

var gun = null

const INIT_LASER_SIZE = 10
var laser_size = -1

# With this way laser will slice through both shield and player same time
var body_list = []
var area_list = []


func _ready():
	# Really need to do the whole bots/players diff
	gun = globals.node_Players.get_node("player_" + String(id_origin) + "/GunAnchor/Gun")
	if gun == null:
		gun = globals.node_Players.get_node("bot_" + String(id_origin) + "/GunAnchor/Gun")
	
	
	gun.connect_laser(self)
	resize(INIT_LASER_SIZE)
	laser_size = INIT_LASER_SIZE
	
	ammo_id = 3


func _process(delta):
	position = gun.global_position
	rotation = gun.global_rotation
	laser_size += 2 * delta
	rpc("resize", laser_size) # Grow laser longer as it's on


func set_id_origin(id):
	id_origin = id


func damage():
	for body in body_list:
		body.hit(id_origin, damage_per_tick)
	for area in area_list:
		area.hit(damage_per_tick)


puppetsync func resize(size):
	$Sprite.scale.y = size
	shape_owner_get_shape(0,0).extents.y = $Sprite.texture.get_size().y * size / 2
	$CollisionShape2D.position.y = -1 * $Sprite.texture.get_size().y * size / 2 # This may or may not be correct, but seems to work :D


func _on_LaserBeam_body_entered(body):
	if body.has_method("hit") and not body_list.has(body):
		body_list.append(body)


func _on_LaserBeam_body_exited(body):
	if body_list.has(body):
		body_list.erase(body)


# Currently don't care who shoot at shields
func _on_LaserBeam_area_entered(area):
	if area.has_method("hit") and not area_list.has(area):
		area_list.append(area)


func _on_LaserBeam_area_exited(area):
	if area_list.has(area):
		area_list.erase(area)

