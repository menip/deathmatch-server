This is the server companion project to https://gitlab.com/menip/deathmatch.

This project is the result of wanting to learn networking in Godot. As such, there may be ideas that could be implemented better. The project is under developement, and I expect to refactor once some QoL stuff is finished client side.

PRs welcome in future, as for now I'd like to refactor first.
