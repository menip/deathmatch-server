extends Sprite

class_name Gun # TODO: add , "path/to/clas/image"

export(String) var description = ""

export(PackedScene) var Ammo

export(int) var MAX_BULLETS = 25 # Note: can't export consts
var can_fire = true

var id_player = -1
var bullet_count = -1

export(int) var ammo_id = -1


func _ready():
	bullet_count = MAX_BULLETS
	
	rpc_id(id_player, "ammo_change", bullet_count)


remote func fire():
	var caller_id = get_tree().get_rpc_sender_id()
	
	if (caller_id == 0 or caller_id == id_player) and can_fire:
		for bullet_pos in $BulletSpread.get_children():
			if bullet_count > 0:
				
				globals.node_world.spawn_bullet(global_position + (bullet_pos.position).rotated(global_rotation), 
						global_rotation, id_player, ammo_id)
				
				bullet_count -= 1
				
		rearm()
		rpc_id(id_player, "ammo_change", bullet_count)
		rpc_id(id_player, "rearm")


func refresh():
	bullet_count = MAX_BULLETS
	
	rpc_id(id_player, "ammo_change", bullet_count)
	can_fire = true
	rpc_id(id_player, "refresh")


func ammo_pickup(amount):
	bullet_count += amount
	
	if bullet_count > MAX_BULLETS:
		bullet_count = MAX_BULLETS
	
	rpc_id(id_player, "ammo_change", bullet_count)

puppetsync func rearm():
	can_fire = false
	$FireDelay.start()


func _on_FireDelay_timeout():
	can_fire = true

func on_queue_free():
	pass