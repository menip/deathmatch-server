extends Gun

var laser_beam = null
var gun_id = 3 # Yes it's hack. Copying from database file

remote func fire():
	var caller_id = get_tree().get_rpc_sender_id()
	
	if caller_id == id_player and can_fire and laser_beam == null and bullet_count > 0:
		for bullet_pos in $BulletSpread.get_children():
			if bullet_count > 0:
				
				globals.node_world.spawn_bullet(global_position + (bullet_pos.position).rotated(global_rotation), 
						global_rotation, id_player, ammo_id)
				
				bullet_count -= 1
		rearm()
		rpc_id(id_player, "ammo_change", bullet_count)
		rpc_id(id_player, "rearm")
		
		$DamageTick.start()


remote func turn_off_laser():
	var caller_id = get_tree().get_rpc_sender_id()
	
	if caller_id == id_player:
		disconnect_laser()


func connect_laser(_laser_beam):
	laser_beam = _laser_beam


func disconnect_laser():
	if laser_beam != null: # in case sync issues
		laser_beam.remove_bullet()
		laser_beam = null
		
		$DamageTick.stop()
		
		rpc("disconnect_laser")

func _on_DamageTick_timeout():
	laser_beam.damage()
	bullet_count -= laser_beam.laser_size / laser_beam.INIT_LASER_SIZE # Consumes more ammo longer it's turned on 
	
	if bullet_count <=0:
		bullet_count = 0
	
	rpc_id(id_player, "ammo_change", bullet_count)
	
	if bullet_count ==0:
		$DamageTick.stop()
		disconnect_laser()


func on_queue_free():
	if laser_beam != null:
		laser_beam.remove_bullet()

