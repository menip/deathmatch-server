extends Pickup

var shield_ammount = 50


func _ready():
	crate_id = 2


func _on_ShieldCrate_body_entered(body):
	if body.has_method("shield_pickup"):
		body.shield_pickup(shield_ammount)
		rpc("pickup")