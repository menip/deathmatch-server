extends Area2D

class_name Pickup # TODO: add , "path/to/class/image"

var is_hidden = false
var crate_id

signal pickup(at_position, crate_type)


puppetsync func pickup():
	emit_signal("pickup", position, crate_id)
	
	queue_free()